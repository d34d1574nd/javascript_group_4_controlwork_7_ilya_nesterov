import React from 'react';
import Price from "../Price/Price";


const ListProduct = props => {
    return (
        props.list.map((name, index) => {
            return (
                <Price
                    key={index}
                    name={name.name}
                    count={name.count}
                >
                    <button className="delete" onClick={() => props.remove(name)}><i className="far fa-trash-alt"></i></button>
                </Price>
            )
        })
    )
};

export default ListProduct