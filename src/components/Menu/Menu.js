import React from 'react';
import './Menu.css';

import Burger from '../../assets/burger.jpg';
import Hamburger from '../../assets/hamburger.jpg';
import CheeseBurger from '../../assets/cheeseburger.png';
import Shaurma from '../../assets/shaurma.jpg';
import Cola from '../../assets/cola.jpg';
import Coffee from '../../assets/coffee.jpg';
import Tea from '../../assets/hottea.jpg';

const Menu = props => {
    const menu = [
        {class: 'food', name: 'burger', price: 120, image: Burger},
        {class: 'food', name: 'cheeseburger', price: 100, image: CheeseBurger},
        {class: 'food', name: 'hamburger', price: 150, image: Hamburger},
        {class: 'food', name: 'shaurma', price: 170, image: Shaurma},
        {class: 'food-drink', name: 'tea', price: 20, image: Tea},
        {class: 'food-drink', name: 'coffee', price: 40, image: Coffee},
        {class: 'food-drink', name: 'coca-cola', price: 40, image: Cola},
    ];

    return (
        <div className="menu">
            {
                menu.map((item, index) => {
                    return (
                        <div key={index} className={item.class} >
                            <button onClick={() => props.addProduct(item)}><img className="img" alt={item.name} src={item.image}/></button>
                            <p className="priceFood">{item.price} KGS</p>
                            <p className="nameProduct">{item.name}</p>
                        </div>
                    )
                })}
        </div>
    )
};

export default Menu
