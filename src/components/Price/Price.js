import React from 'react';
import './Price.css';

const Price = props => {
    const getProduct = () => {
        const menu = [];
        for (let i = 0; i < props.count; i++) {
            menu.splice(0,1, props.count)
        }
        return menu;
    };
    return (
        getProduct().map((name, index) => {
        return (
            <div key={index}>
                <p className="name">{props.name}</p>
                <p className="count">  {props.count}</p>
                {props.children}
            </div>
        )
}))
};

export default Price