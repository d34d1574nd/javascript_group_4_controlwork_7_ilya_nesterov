import React, { Component } from 'react';
import './App.css';
import Menu from '../components/Menu/Menu';
import ListProduct from '../components/ListProduct/ListProduct'

class App extends Component {
    state = {
        menu: [
            {name: 'burger', price: 120, count: 0},
            {name: 'cheeseburger', price: 100, count: 0},
            {name: 'hamburger', price: 150, count: 0},
            {name: 'shaurma', price: 170, count: 0},
            {name: 'tea', price: 20, count: 0},
            {name: 'coffee', price: 40, count: 0},
            {name: 'coca-cola', price: 40, count: 0},
        ],
        totalPrice: 0,
    };

    noOrder = ['noOrder'];

     addProduct = fill => {
         this.noOrder = ['Order'];
        const menuCopy = [...this.state.menu];
        let totalPriceCopy = this.state.totalPrice;
        for (let i = 0; i < menuCopy.length; i ++) {
            if (menuCopy[i].name === fill.name) {
                menuCopy[i].count++;
                totalPriceCopy += menuCopy[i].price;
            }
        }
        this.setState({ingredients: menuCopy, totalPrice: totalPriceCopy});
    };

    remove = fill => {
        const menuCopy = [...this.state.menu];
        let totalPriceCopy = this.state.totalPrice;
        for (let i = 0; i < menuCopy.length; i ++) {
            if (menuCopy[i].name === fill.name && menuCopy[i].count !== 0) {
                menuCopy[i].count--;
                totalPriceCopy -= fill.price;
            }
        }
        this.setState({menu: menuCopy, totalPrice: totalPriceCopy});
    };


    render() {
        return (
            <div className="App">
                    <Menu
                        addProduct={(name) => this.addProduct(name)}
                    />
                    <div className="fastFood">
                        <p className="order">Order details:</p>
                        <p className={this.noOrder}>There is no order!!!</p>
                        <p className={this.noOrder}>Please add product</p>
                        <ListProduct
                            list={this.state.menu}
                            count={this.state.menu}
                            remove={(name) => this.remove(name)}
                        />
                        <p className="quantityPrice">Price: {this.state.totalPrice} KGS</p>
                        <hr/>
                        <span className="forget">Please do not forget to take the check from the administrator</span>
                    </div>
                </div>
        );
    }
}

export default App;
